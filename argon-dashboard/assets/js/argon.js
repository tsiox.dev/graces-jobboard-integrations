
/*!

=========================================================
* Argon Dashboard - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md)

* Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/



//
// Layout
//

'use strict';

var Layout = (function() {

    function pinSidenav() {
        $('.sidenav-toggler').addClass('active');
        $('.sidenav-toggler').data('action', 'sidenav-unpin');
        $('body').removeClass('g-sidenav-hidden').addClass('g-sidenav-show g-sidenav-pinned');
        $('body').append('<div class="backdrop d-xl-none" data-action="sidenav-unpin" data-target='+$('#sidenav-main').data('target')+' />');

        // Store the sidenav state in a cookie session
        Cookies.set('sidenav-state', 'pinned');
    }

    function unpinSidenav() {
        $('.sidenav-toggler').removeClass('active');
        $('.sidenav-toggler').data('action', 'sidenav-pin');
        $('body').removeClass('g-sidenav-pinned').addClass('g-sidenav-hidden');
        $('body').find('.backdrop').remove();

        // Store the sidenav state in a cookie session
        Cookies.set('sidenav-state', 'unpinned');
    }

    // Set sidenav state from cookie

    var $sidenavState = Cookies.get('sidenav-state') ? Cookies.get('sidenav-state') : 'pinned';

    if($(window).width() > 1200) {
        if($sidenavState == 'pinned') {
            pinSidenav()
        }

        if(Cookies.get('sidenav-state') == 'unpinned') {
            unpinSidenav()
        }

        $(window).resize(function() {
            if( $('body').hasClass('g-sidenav-show') && !$('body').hasClass('g-sidenav-pinned')) {
                $('body').removeClass('g-sidenav-show').addClass('g-sidenav-hidden');
            }
        })
    }

    if($(window).width() < 1200){
      $('body').removeClass('g-sidenav-hide').addClass('g-sidenav-hidden');
      $('body').removeClass('g-sidenav-show');
      $(window).resize(function() {
          if( $('body').hasClass('g-sidenav-show') && !$('body').hasClass('g-sidenav-pinned')) {
              $('body').removeClass('g-sidenav-show').addClass('g-sidenav-hidden');
          }
      })
    }



    $("body").on("click", "[data-action]", function(e) {

        e.preventDefault();

        var $this = $(this);
        var action = $this.data('action');
        var target = $this.data('target');


        // Manage actions

        switch (action) {
            case 'sidenav-pin':
                pinSidenav();
            break;

            case 'sidenav-unpin':
                unpinSidenav();
            break;

            case 'search-show':
                target = $this.data('target');
                $('body').removeClass('g-navbar-search-show').addClass('g-navbar-search-showing');

                setTimeout(function() {
                    $('body').removeClass('g-navbar-search-showing').addClass('g-navbar-search-show');
                }, 150);

                setTimeout(function() {
                    $('body').addClass('g-navbar-search-shown');
                }, 300)
            break;

            case 'search-close':
                target = $this.data('target');
                $('body').removeClass('g-navbar-search-shown');

                setTimeout(function() {
                    $('body').removeClass('g-navbar-search-show').addClass('g-navbar-search-hiding');
                }, 150);

                setTimeout(function() {
                    $('body').removeClass('g-navbar-search-hiding').addClass('g-navbar-search-hidden');
                }, 300);

                setTimeout(function() {
                    $('body').removeClass('g-navbar-search-hidden');
                }, 500);
            break;
        }
    })


    // Add sidenav modifier classes on mouse events

    $('.sidenav').on('mouseenter', function() {
        if(! $('body').hasClass('g-sidenav-pinned')) {
            $('body').removeClass('g-sidenav-hide').removeClass('g-sidenav-hidden').addClass('g-sidenav-show');
        }
    })

    $('.sidenav').on('mouseleave', function() {
        if(! $('body').hasClass('g-sidenav-pinned')) {
            $('body').removeClass('g-sidenav-show').addClass('g-sidenav-hide');

            setTimeout(function() {
                $('body').removeClass('g-sidenav-hide').addClass('g-sidenav-hidden');
            }, 300);
        }
    })


    // Make the body full screen size if it has not enough content inside
    $(window).on('load resize', function() {
        if($('body').height() < 800) {
            $('body').css('min-height', '100vh');
            $('#footer-main').addClass('footer-auto-bottom')
        }
    })

})();

//
// Charts
//

'use strict';

var Charts = (function() {

	// Variable

	var $toggle = $('[data-toggle="chart"]');
	var mode = 'light';//(themeMode) ? themeMode : 'light';
	var fonts = {
		base: 'Open Sans'
	}

	// Colors
	var colors = {
		gray: {
			100: '#f6f9fc',
			200: '#e9ecef',
			300: '#dee2e6',
			400: '#ced4da',
			500: '#adb5bd',
			600: '#8898aa',
			700: '#525f7f',
			800: '#32325d',
			900: '#212529'
		},
		theme: {
			'default': '#172b4d',
			'primary': 'rgb(91, 55, 91, 0.5)',
			'secondary': '#f4f5f7',
			'info': 'rgb(91, 55, 91, 0.5)',
			'success': '#2dce89',
			'danger': '#f5365c',
			'warning': '#fb6340'
		},
		black: '#12263F',
		white: '#FFFFFF',
		transparent: 'transparent',
	};


	// Methods

	// Chart.js global options
	function chartOptions() {

		// Options
		var options = {
			defaults: {
				global: {
					responsive: true,
					maintainAspectRatio: false,
					defaultColor: (mode == 'dark') ? colors.gray[700] : colors.gray[600],
					defaultFontColor: (mode == 'dark') ? colors.gray[700] : colors.gray[600],
					defaultFontFamily: fonts.base,
					defaultFontSize: 13,
					layout: {
						padding: 0
					},
					legend: {
						display: false,
						position: 'bottom',
						labels: {
							usePointStyle: true,
							padding: 16
						}
					},
					elements: {
						point: {
							radius: 0,
							backgroundColor: colors.theme['primary']
						},
						line: {
							tension: .4,
							borderWidth: 4,
							borderColor: colors.theme['primary'],
							backgroundColor: colors.transparent,
							borderCapStyle: 'rounded'
						},
						rectangle: {
							backgroundColor: colors.theme['warning']
						},
						arc: {
							backgroundColor: colors.theme['primary'],
							borderColor: (mode == 'dark') ? colors.gray[800] : colors.white,
							borderWidth: 4
						}
					},
					tooltips: {
						enabled: true,
						mode: 'index',
						intersect: false,
					}
				},
				doughnut: {
					cutoutPercentage: 83,
					legendCallback: function(chart) {
						var data = chart.data;
						var content = '';

						data.labels.forEach(function(label, index) {
							var bgColor = data.datasets[0].backgroundColor[index];

							content += '<span class="chart-legend-item">';
							content += '<i class="chart-legend-indicator" style="background-color: ' + bgColor + '"></i>';
							content += label;
							content += '</span>';
						});

						return content;
					}
				}
			}
		}

		// yAxes
		Chart.scaleService.updateScaleDefaults('linear', {
			gridLines: {
				borderDash: [2],
				borderDashOffset: [2],
				color: (mode == 'dark') ? colors.gray[900] : colors.gray[300],
				drawBorder: false,
				drawTicks: false,
				drawOnChartArea: true,
				zeroLineWidth: 0,
				zeroLineColor: 'rgba(0,0,0,0)',
				zeroLineBorderDash: [2],
				zeroLineBorderDashOffset: [2]
			},
			ticks: {
				beginAtZero: true,
				padding: 10,
				callback: function(value) {
					if (!(value % 10)) {
						return value
					}
				}
			}
		});

		// xAxes
		Chart.scaleService.updateScaleDefaults('category', {
			gridLines: {
				drawBorder: false,
				drawOnChartArea: false,
				drawTicks: false
			},
			ticks: {
				padding: 20
			},
			maxBarThickness: 10
		});

		return options;

	}

	// Parse global options
	function parseOptions(parent, options) {
		for (var item in options) {
			if (typeof options[item] !== 'object') {
				parent[item] = options[item];
			} else {
				parseOptions(parent[item], options[item]);
			}
		}
	}

	// Push options
	function pushOptions(parent, options) {
		for (var item in options) {
			if (Array.isArray(options[item])) {
				options[item].forEach(function(data) {
					parent[item].push(data);
				});
			} else {
				pushOptions(parent[item], options[item]);
			}
		}
	}

	// Pop options
	function popOptions(parent, options) {
		for (var item in options) {
			if (Array.isArray(options[item])) {
				options[item].forEach(function(data) {
					parent[item].pop();
				});
			} else {
				popOptions(parent[item], options[item]);
			}
		}
	}

	// Toggle options
	function toggleOptions(elem) {
		var options = elem.data('add');
		var $target = $(elem.data('target'));
		var $chart = $target.data('chart');

		if (elem.is(':checked')) {

			// Add options
			pushOptions($chart, options);

			// Update chart
			$chart.update();
		} else {

			// Remove options
			popOptions($chart, options);

			// Update chart
			$chart.update();
		}
	}

	// Update options
	function updateOptions(elem) {
		var options = elem.data('update');
		var $target = $(elem.data('target'));
		var $chart = $target.data('chart');

		// Parse options
		parseOptions($chart, options);

		// Toggle ticks
		toggleTicks(elem, $chart);

		// Update chart
		$chart.update();
	}

	// Toggle ticks
	function toggleTicks(elem, $chart) {

		if (elem.data('prefix') !== undefined || elem.data('prefix') !== undefined) {
			var prefix = elem.data('prefix') ? elem.data('prefix') : '';
			var suffix = elem.data('suffix') ? elem.data('suffix') : '';

			// Update ticks
			$chart.options.scales.yAxes[0].ticks.callback = function(value) {
				if (!(value % 10)) {
					return prefix + value + suffix;
				}
			}

			// Update tooltips
			$chart.options.tooltips.callbacks.label = function(item, data) {
				var label = data.datasets[item.datasetIndex].label || '';
				var yLabel = item.yLabel;
				var content = '';

				if (data.datasets.length > 1) {
					content += '<span class="popover-body-label mr-auto">' + label + '</span>';
				}

				content += '<span class="popover-body-value">' + prefix + yLabel + suffix + '</span>';
				return content;
			}

		}
	}


	// Events

	// Parse global options
	if (window.Chart) {
		parseOptions(Chart, chartOptions());
	}

	// Toggle options
	$toggle.on({
		'change': function() {
			var $this = $(this);

			if ($this.is('[data-add]')) {
				toggleOptions($this);
			}
		},
		'click': function() {
			var $this = $(this);

			if ($this.is('[data-update]')) {
				updateOptions($this);
			}
		}
	});


	// Return

	return {
		colors: colors,
		fonts: fonts,
		mode: mode
	};

})();

//
// Icon code copy/paste
//

'use strict';

var CopyIcon = (function() {

	// Variables

	var $element = '.btn-icon-clipboard',
		$btn = $($element);


	// Methods

	function init($this) {
		$this.tooltip().on('mouseleave', function() {
			// Explicitly hide tooltip, since after clicking it remains
			// focused (as it's a button), so tooltip would otherwise
			// remain visible until focus is moved away
			$this.tooltip('hide');
		});

		var clipboard = new ClipboardJS($element);

		clipboard.on('success', function(e) {
			$(e.trigger)
				.attr('title', 'Copied!')
				.tooltip('_fixTitle')
				.tooltip('show')
				.attr('title', 'Copy to clipboard')
				.tooltip('_fixTitle')

			e.clearSelection()
		});
	}


	// Events
	if ($btn.length) {
		init($btn);
	}

})();

//
// Navbar
//

'use strict';

var Navbar = (function() {

	// Variables

	var $nav = $('.navbar-nav, .navbar-nav .nav');
	var $collapse = $('.navbar .collapse');
	var $dropdown = $('.navbar .dropdown');

	// Methods

	function accordion($this) {
		$this.closest($nav).find($collapse).not($this).collapse('hide');
	}

    function closeDropdown($this) {
        var $dropdownMenu = $this.find('.dropdown-menu');

        $dropdownMenu.addClass('close');

    	setTimeout(function() {
    		$dropdownMenu.removeClass('close');
    	}, 200);
	}


	// Events

	$collapse.on({
		'show.bs.collapse': function() {
			accordion($(this));
		}
	})

	$dropdown.on({
		'hide.bs.dropdown': function() {
			closeDropdown($(this));
		}
	})

})();


//
// Navbar collapse
//


var NavbarCollapse = (function() {

	// Variables

	var $nav = $('.navbar-nav'),
		$collapse = $('.navbar .navbar-custom-collapse');


	// Methods

	function hideNavbarCollapse($this) {
		$this.addClass('collapsing-out');
	}

	function hiddenNavbarCollapse($this) {
		$this.removeClass('collapsing-out');
	}


	// Events

	if ($collapse.length) {
		$collapse.on({
			'hide.bs.collapse': function() {
				hideNavbarCollapse($collapse);
			}
		})

		$collapse.on({
			'hidden.bs.collapse': function() {
				hiddenNavbarCollapse($collapse);
			}
		})
	}

	var navbar_menu_visible = 0;

	$( ".sidenav-toggler" ).click(function() {
		if(navbar_menu_visible == 1){
		  $('body').removeClass('nav-open');
			navbar_menu_visible = 0;
			$('.bodyClick').remove();

		} else {

		var div = '<div class="bodyClick"></div>';
		$(div).appendTo('body').click(function() {
				 $('body').removeClass('nav-open');
					navbar_menu_visible = 0;
					$('.bodyClick').remove();
					
			 });

		 $('body').addClass('nav-open');
			navbar_menu_visible = 1;

		}

	});

})();

//
// Popover
//

'use strict';

var Popover = (function() {

	// Variables

	var $popover = $('[data-toggle="popover"]'),
		$popoverClass = '';


	// Methods

	function init($this) {
		if ($this.data('color')) {
			$popoverClass = 'popover-' + $this.data('color');
		}

		var options = {
			trigger: 'focus',
			template: '<div class="popover ' + $popoverClass + '" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
		};

		$this.popover(options);
	}


	// Events

	if ($popover.length) {
		$popover.each(function() {
			init($(this));
		});
	}

})();

//
// Scroll to (anchor links)
//

'use strict';

var ScrollTo = (function() {

	//
	// Variables
	//

	var $scrollTo = $('.scroll-me, [data-scroll-to], .toc-entry a');


	//
	// Methods
	//

	function scrollTo($this) {
		var $el = $this.attr('href');
        var offset = $this.data('scroll-to-offset') ? $this.data('scroll-to-offset') : 0;
		var options = {
			scrollTop: $($el).offset().top - offset
		};

        // Animate scroll to the selected section
        $('html, body').stop(true, true).animate(options, 600);

        event.preventDefault();
	}


	//
	// Events
	//

	if ($scrollTo.length) {
		$scrollTo.on('click', function(event) {
			scrollTo($(this));
		});
	}

})();

//
// Tooltip
//

'use strict';

var Tooltip = (function() {

	// Variables

	var $tooltip = $('[data-toggle="tooltip"]');


	// Methods

	function init() {
		$tooltip.tooltip();
	}


	// Events

	if ($tooltip.length) {
		init();
	}

})();

//
// Form control
//

'use strict';

var FormControl = (function() {

	// Variables

	var $input = $('.form-control');


	// Methods

	function init($this) {
		$this.on('focus blur', function(e) {
        $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus'));
    }).trigger('blur');
	}


	// Events

	if ($input.length) {
		init($input);
	}

})();

//
// Google maps
//

var $map = $('#map-default'),
    map,
    lat,
    lng,
    color = "rgb(91, 55, 91, 0.5)";

function initMap() {

    map = document.getElementById('map-default');
    lat = map.getAttribute('data-lat');
    lng = map.getAttribute('data-lng');

    var myLatlng = new google.maps.LatLng(lat, lng);
    var mapOptions = {
        zoom: 12,
        scrollwheel: false,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    }

    map = new google.maps.Map(map, mapOptions);

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        animation: google.maps.Animation.DROP,
        title: 'Hello World!'
    });

    var contentString = '<div class="info-window-content"><h2>Argon Dashboard</h2>' +
        '<p>A beautiful Dashboard for Bootstrap 4. It is Free and Open Source.</p></div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map, marker);
    });
}

if($map.length) {
    google.maps.event.addDomListener(window, 'load', initMap);
}

//
// Bars chart
//

var BarsChart = (function() {

	//
	// Variables
	//

	var $chart = $('#chart-bars');


	//
	// Methods
	//

	// Init chart
	function initChart($chart) {

		// Create chart
		var ordersChart = new Chart($chart, {
			type: 'bar',
			data: {
				labels: ['Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
				datasets: [{
					label: 'Sales',
					data: [25, 20, 30, 22, 17, 29]
				}]
			}
		});

		// Save to jQuery object
		$chart.data('chart', ordersChart);
	}


	// Init chart
	if ($chart.length) {
		initChart($chart);
	}

})();

'use strict';

//
// Sales chart
//

var SalesChart = (function() {

  // Variables

  var $chart = $('#chart-sales-dark');


  // Methods

  function init($chart) {

    var salesChart = new Chart($chart, {
      type: 'line',
      options: {
        scales: {
          yAxes: [{
            gridLines: {
              lineWidth: 1,
              color: Charts.colors.gray[900],
              zeroLineColor: Charts.colors.gray[900]
            },
            ticks: {
              callback: function(value) {
                if (!(value % 10)) {
                  return '$' + value + 'k';
                }
              }
            }
          }]
        },
        tooltips: {
          callbacks: {
            label: function(item, data) {
              var label = data.datasets[item.datasetIndex].label || '';
              var yLabel = item.yLabel;
              var content = '';

              if (data.datasets.length > 1) {
                content += '<span class="popover-body-label mr-auto">' + label + '</span>';
              }

              content += '<span class="popover-body-value">$' + yLabel + 'k</span>';
              return content;
            }
          }
        }
      },
      data: {
        labels: ['May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        datasets: [{
          label: 'Performance',
          data: [0, 20, 10, 30, 15, 40, 20, 60, 60]
        }]
      }
    });

    // Save to jQuery object

    $chart.data('chart', salesChart);

  };


  // Events

  if ($chart.length) {
    init($chart);
  }

})();

//
// Bootstrap Datepicker
//

'use strict';

var Datepicker = (function() {

	// Variables

	var $datepicker = $('.datepicker');


	// Methods

	function init($this) {
		var options = {
			disableTouchKeyboard: true,
			autoclose: false
		};

		$this.datepicker(options);
	}


	// Events

	if ($datepicker.length) {
		$datepicker.each(function() {
			init($(this));
		});
	}

})();

//
// Form control
//

'use strict';

var noUiSlider = (function() {

	// Variables

	// var $sliderContainer = $('.input-slider-container'),
	// 		$slider = $('.input-slider'),
	// 		$sliderId = $slider.attr('id'),
	// 		$sliderMinValue = $slider.data('range-value-min');
	// 		$sliderMaxValue = $slider.data('range-value-max');;


	// // Methods
	//
	// function init($this) {
	// 	$this.on('focus blur', function(e) {
  //       $this.parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
  //   }).trigger('blur');
	// }
	//
	//
	// // Events
	//
	// if ($input.length) {
	// 	init($input);
	// }



	if ($(".input-slider-container")[0]) {
			$('.input-slider-container').each(function() {

					var slider = $(this).find('.input-slider');
					var sliderId = slider.attr('id');
					var minValue = slider.data('range-value-min');
					var maxValue = slider.data('range-value-max');

					var sliderValue = $(this).find('.range-slider-value');
					var sliderValueId = sliderValue.attr('id');
					var startValue = sliderValue.data('range-value-low');

					var c = document.getElementById(sliderId),
							d = document.getElementById(sliderValueId);

					noUiSlider.create(c, {
							start: [parseInt(startValue)],
							connect: [true, false],
							//step: 1000,
							range: {
									'min': [parseInt(minValue)],
									'max': [parseInt(maxValue)]
							}
					});

					c.noUiSlider.on('update', function(a, b) {
							d.textContent = a[b];
					});
			})
	}

	if ($("#input-slider-range")[0]) {
			var c = document.getElementById("input-slider-range"),
					d = document.getElementById("input-slider-range-value-low"),
					e = document.getElementById("input-slider-range-value-high"),
					f = [d, e];

			noUiSlider.create(c, {
					start: [parseInt(d.getAttribute('data-range-value-low')), parseInt(e.getAttribute('data-range-value-high'))],
					connect: !0,
					range: {
							min: parseInt(c.getAttribute('data-range-value-min')),
							max: parseInt(c.getAttribute('data-range-value-max'))
					}
			}), c.noUiSlider.on("update", function(a, b) {
					f[b].textContent = a[b]
			})
	}

})();

//
// Scrollbar
//

'use strict';


function contrat() {
document.getElementById("slide-anime").style.top = "55px";
document.getElementById("slide-anime").style.opacity = "1";
document.getElementById("slide-anime").style.transition = "0.5s ease";
document.getElementById("contrat").style.display = "block";
document.getElementById("expertise").style.display = "none";
document.getElementById("activite").style.display = "none";
document.getElementById("maitrise").style.display = "none";
document.getElementById("competence").style.display = "none";
document.getElementById("langue").style.display = "none";
document.getElementById("annee-experience").style.display = "none";
document.getElementById("travail").style.display = "none";
document.getElementById("remuneration-free").style.display = "none";
document.getElementById("disponible").style.display = "none";
document.getElementById("remuneration-all").style.display = "none";
document.getElementById("inscription").style.display = "none";
document.getElementById("condition-vente").style.display = "none";

}
function reconnu() {
document.getElementById("slide-anime").style.top = "100px";
document.getElementById("slide-anime").style.transition = "0.5s ease";
document.getElementById("expertise").style.display = "block";
document.getElementById("contrat").style.display = "none";
document.getElementById("activite").style.display = "none";
document.getElementById("maitrise").style.display = "none";
document.getElementById("competence").style.display = "none";
document.getElementById("langue").style.display = "none";
document.getElementById("annee-experience").style.display = "none";
document.getElementById("travail").style.display = "none";
document.getElementById("remuneration-free").style.display = "none";
document.getElementById("disponible").style.display = "none";
document.getElementById("remuneration-all").style.display = "none";
document.getElementById("inscription").style.display = "none";
document.getElementById("condition-vente").style.display = "none";

}
function activite() {
	document.getElementById("slide-anime").style.opacity = "1";
document.getElementById("slide-anime").style.top = "145px";
document.getElementById("slide-anime").style.transition = "0.5s ease";
document.getElementById("activite").style.display = "block";
document.getElementById("contrat").style.display = "none";
document.getElementById("expertise").style.display = "none";
document.getElementById("maitrise").style.display = "none";
document.getElementById("competence").style.display = "none";
document.getElementById("langue").style.display = "none";
document.getElementById("annee-experience").style.display = "none";
document.getElementById("travail").style.display = "none";
document.getElementById("remuneration-free").style.display = "none";
document.getElementById("disponible").style.display = "none";
document.getElementById("remuneration-all").style.display = "none";
document.getElementById("inscription").style.display = "none";
document.getElementById("condition-vente").style.display = "none";
}
function maitrise() {
document.getElementById("slide-anime").style.opacity = "1";
document.getElementById("slide-anime").style.top = "190px";
document.getElementById("slide-anime").style.transition = "0.5s ease";
document.getElementById("maitrise").style.display = "block";
document.getElementById("activite").style.display = "none";
document.getElementById("contrat").style.display = "none";
document.getElementById("expertise").style.display = "none";
document.getElementById("competence").style.display = "none";
document.getElementById("langue").style.display = "none";
document.getElementById("annee-experience").style.display = "none";
document.getElementById("travail").style.display = "none";
document.getElementById("remuneration-free").style.display = "none";
document.getElementById("inscription").style.display = "none";
document.getElementById("disponible").style.display = "none";
document.getElementById("remuneration-all").style.display = "none";
document.getElementById("condition-vente").style.display = "none";
}
function regle() {
document.getElementById("slide-anime").style.top = "235px";
document.getElementById("slide-anime").style.transition = "0.5s ease";
document.getElementById("competence").style.display = "block";
document.getElementById("maitrise").style.display = "none";
document.getElementById("activite").style.display = "none";
document.getElementById("contrat").style.display = "none";
document.getElementById("expertise").style.display = "none";
document.getElementById("langue").style.display = "none";
document.getElementById("annee-experience").style.display = "none";
document.getElementById("travail").style.display = "none";
document.getElementById("remuneration-free").style.display = "none";
document.getElementById("disponible").style.display = "none";
document.getElementById("inscription").style.display = "none";
document.getElementById("remuneration-all").style.display = "none";
}
function langue() {
document.getElementById("slide-anime").style.top = "280px";
document.getElementById("slide-anime").style.transition = "0.5s ease";
document.getElementById("langue").style.display = "block";
document.getElementById("competence").style.display = "none";
document.getElementById("maitrise").style.display = "none";
document.getElementById("activite").style.display = "none";
document.getElementById("contrat").style.display = "none";
document.getElementById("expertise").style.display = "none";
document.getElementById("annee-experience").style.display = "none";
document.getElementById("travail").style.display = "none";
document.getElementById("remuneration-free").style.display = "none";
document.getElementById("disponible").style.display = "none";
document.getElementById("inscription").style.display = "none";
document.getElementById("remuneration-all").style.display = "none";

}
function expert() {
document.getElementById("slide-anime").style.top = "325px";
document.getElementById("slide-anime").style.transition = "0.5s ease";
document.getElementById("annee-experience").style.display = "block";
document.getElementById("langue").style.display = "none";
document.getElementById("competence").style.display = "none";
document.getElementById("maitrise").style.display = "none";
document.getElementById("activite").style.display = "none";
document.getElementById("contrat").style.display = "none";
document.getElementById("expertise").style.display = "none";
document.getElementById("travail").style.display = "none";
document.getElementById("remuneration-free").style.display = "none";
document.getElementById("disponible").style.display = "none";
document.getElementById("inscription").style.display = "none";
document.getElementById("remuneration-all").style.display = "none";
}
function travail() {
	document.getElementById("pourcentage-primary").style.width = "70%";
document.getElementById("slide-anime").style.top = "370px";
document.getElementById("slide-anime").style.transition = "0.5s ease";
document.getElementById("annee-experience").style.display = "none";
document.getElementById("langue").style.display = "none";
document.getElementById("competence").style.display = "none";
document.getElementById("maitrise").style.display = "none";
document.getElementById("activite").style.display = "none";
document.getElementById("contrat").style.display = "none";
document.getElementById("expertise").style.display = "none";
document.getElementById("remuneration-free").style.display = "none";
document.getElementById("remuneration-all").style.display = "none";
document.getElementById("disponible").style.display = "none";
document.getElementById("inscription").style.display = "none";
document.getElementById("travail").style.display = "block";
}
function remuneration() {
document.getElementById("slide-anime").style.top = "415px";
document.getElementById("slide-anime").style.transition = "0.5s ease";
document.getElementById("annee-experience").style.display = "none";
document.getElementById("langue").style.display = "none";
document.getElementById("competence").style.display = "none";
document.getElementById("maitrise").style.display = "none";
document.getElementById("activite").style.display = "none";
document.getElementById("contrat").style.display = "none";
document.getElementById("expertise").style.display = "none";
document.getElementById("travail").style.display = "none";
document.getElementById("remuneration-all").style.display = "none";
document.getElementById("disponible").style.display = "none";
document.getElementById("inscription").style.display = "none";
document.getElementById("remuneration-free").style.display = "block";

}
function remunerationAll() {
	
document.getElementById("remuneration-free").style.display = "none";
document.getElementById("remuneration-all").style.display = "block";
}
function disponible() {
document.getElementById("slide-anime").style.top = "460px";
document.getElementById("slide-anime").style.transition = "0.5s ease";
document.getElementById("annee-experience").style.display = "none";
document.getElementById("langue").style.display = "none";
document.getElementById("competence").style.display = "none";
document.getElementById("maitrise").style.display = "none";
document.getElementById("activite").style.display = "none";
document.getElementById("contrat").style.display = "none";
document.getElementById("expertise").style.display = "none";
document.getElementById("travail").style.display = "none";
document.getElementById("remuneration-all").style.display = "none";
document.getElementById("remuneration-free").style.display = "none";
document.getElementById("inscription").style.display = "none";
document.getElementById("disponible").style.display = "block";
}
function inscription() {
document.getElementById("slide-anime").style.top = "505px";
document.getElementById("slide-anime").style.transition = "0.5s ease";
document.getElementById("inscription").style.display = "block";
document.getElementById("annee-experience").style.display = "none";
document.getElementById("langue").style.display = "none";
document.getElementById("competence").style.display = "none";
document.getElementById("maitrise").style.display = "none";
document.getElementById("activite").style.display = "none";
document.getElementById("contrat").style.display = "none";
document.getElementById("expertise").style.display = "none";
document.getElementById("travail").style.display = "none";
document.getElementById("remuneration-all").style.display = "none";
document.getElementById("remuneration-free").style.display = "none";
document.getElementById("disponible").style.display = "none";
}
function condition() {
document.getElementById("slide-anime").style.opacity = "0";
document.getElementById("slide-anime").style.transition = "0.5s ease";
document.getElementById("condition-vente").style.display = "block";
document.getElementById("activite").style.display = "none";
document.getElementById("contrat").style.display = "none";
document.getElementById("expertise").style.display = "none";
}

function fermerKys() {
document.getElementById("kys").style.display = "none";
}
function fermerKys1() {
document.getElementById("kys1").style.display = "none";
}
function fermerKys2() {
document.getElementById("kys2").style.display = "none";
}
function fermerKys3() {
document.getElementById("kys3").style.display = "none";
}
function fermerRisk() {
document.getElementById("risk").style.display = "none";
}
function fermerRisk1() {
document.getElementById("risk1").style.display = "none";
}
function fermerRisk2() {
document.getElementById("risk2").style.display = "none";
}
function fermerRisk3() {
document.getElementById("risk3").style.display = "none";
}
function fermerGouvernance() {
document.getElementById("gouvernance").style.display = "none";
}
function fermerManagement() {
document.getElementById("management").style.display = "none";
}
function fermerCompliance() {
document.getElementById("compliance").style.display = "none";
}
function fermerDac() {
document.getElementById("dac").style.display = "none";
}

function hoverbox() {
document.getElementById("hoverbox").style.display = "block";
document.getElementById("hoverboxnormal").style.display = "none";

}
function hoverboxnormal() {
document.getElementById("hoverbox").style.display = "none";
document.getElementById("hoverboxnormal").style.display = "block";

}
function help1() {
document.getElementById("help1").style.display = "none";
document.getElementById("help2").style.display = "block";
document.getElementById("list-help").style.display = "block";

}
function help2() {
document.getElementById("help2").style.display = "none";
document.getElementById("list-help").style.display = "none";
document.getElementById("help1").style.display = "block";

}
function tableauBordEnter() {
document.getElementById("tableau-bord").style.display = "block";

}
function tableauBordLeave() {
document.getElementById("tableau-bord").style.display = "none";

}
function displaypop() {
document.getElementById("pop").style.display = "block";

}
function fermerpop() {
document.getElementById("pop").style.display = "none";

}
function fermerpop2() {
document.getElementById("pop2").style.display = "none";

}
function displaypop2() {
document.getElementById("pop2").style.display = "block";

}
function fermerpop3() {
document.getElementById("pop3").style.display = "none";

}
function displaypop3() {
document.getElementById("pop3").style.display = "block";

}
function borderclick1() {
document.getElementById("border-click-1").style.display = "none";
document.getElementById("box-click-1").style.display = "block";

}
function boxclick1() {
document.getElementById("box-click-1").style.display = "none";
document.getElementById("border-click-1").style.display = "block";

}
function borderclick2() {
document.getElementById("border-click-2").style.display = "none";
document.getElementById("box-click-2").style.display = "block";

}
function boxclick2() {
document.getElementById("box-click-2").style.display = "none";
document.getElementById("border-click-2").style.display = "block";

}
function borderclick3() {
document.getElementById("border-click-3").style.display = "none";
document.getElementById("box-click-3").style.display = "block";

}
function boxclick3() {
document.getElementById("box-click-3").style.display = "none";
document.getElementById("border-click-3").style.display = "block";

}
function borderclick4() {
document.getElementById("border-click-4").style.display = "none";
document.getElementById("box-click-4").style.display = "block";

}
function boxclick4() {
document.getElementById("box-click-4").style.display = "none";
document.getElementById("border-click-4").style.display = "block";

}
function borderclick5() {
document.getElementById("border-click-5").style.display = "none";
document.getElementById("box-click-5").style.display = "block";

}
function boxclick5() {
document.getElementById("box-click-5").style.display = "none";
document.getElementById("border-click-5").style.display = "block";

}
function borderclick6() {
document.getElementById("border-click-6").style.display = "none";
document.getElementById("box-click-6").style.display = "block";

}
function boxclick6() {
document.getElementById("box-click-6").style.display = "none";
document.getElementById("border-click-6").style.display = "block";

}
function borderclick7() {
document.getElementById("border-click-7").style.display = "none";
document.getElementById("box-click-7").style.display = "block";

}
function boxclick7() {
document.getElementById("box-click-7").style.display = "none";
document.getElementById("border-click-7").style.display = "block";

}
function borderclick8() {
document.getElementById("border-click-8").style.display = "none";
document.getElementById("box-click-8").style.display = "block";

}
function boxclick8() {
document.getElementById("box-click-8").style.display = "none";
document.getElementById("border-click-8").style.display = "block";

}
function borderclick9() {
document.getElementById("border-click-9").style.display = "none";
document.getElementById("box-click-9").style.display = "block";

}
function boxclick9() {
document.getElementById("box-click-9").style.display = "none";
document.getElementById("border-click-9").style.display = "block";

}
function borderclick10() {
document.getElementById("border-click-10").style.display = "none";
document.getElementById("box-click-10").style.display = "block";

}
function boxclick10() {
document.getElementById("box-click-10").style.display = "none";
document.getElementById("border-click-10").style.display = "block";

}
function borderclick11() {
document.getElementById("border-click-11").style.display = "none";
document.getElementById("box-click-11").style.display = "block";

}
function boxclick11() {
document.getElementById("box-click-11").style.display = "none";
document.getElementById("border-click-11").style.display = "block";

}
function borderclick12() {
document.getElementById("border-click-12").style.display = "none";
document.getElementById("box-click-12").style.display = "block";

}
function boxclick12() {
document.getElementById("box-click-12").style.display = "none";
document.getElementById("border-click-12").style.display = "block";

}
function borderclick13() {
document.getElementById("border-click-13").style.display = "none";
document.getElementById("box-click-13").style.display = "block";

}
function boxclick13() {
document.getElementById("box-click-13").style.display = "none";
document.getElementById("border-click-13").style.display = "block";

}
function borderclick14() {
document.getElementById("border-click-14").style.display = "none";
document.getElementById("box-click-14").style.display = "block";

}
function boxclick14() {
document.getElementById("box-click-14").style.display = "none";
document.getElementById("border-click-14").style.display = "block";

}
function borderclick15() {
document.getElementById("border-click-15").style.display = "none";
document.getElementById("box-click-15").style.display = "block";

}
function boxclick15() {
document.getElementById("box-click-15").style.display = "none";
document.getElementById("border-click-15").style.display = "block";

}
function borderclick16() {
document.getElementById("border-click-16").style.display = "none";
document.getElementById("box-click-16").style.display = "block";

}
function boxclick16() {
document.getElementById("box-click-16").style.display = "none";
document.getElementById("border-click-16").style.display = "block";

}
function borderclick17() {
document.getElementById("border-click-17").style.display = "none";
document.getElementById("box-click-17").style.display = "block";

}
function boxclick17() {
document.getElementById("box-click-17").style.display = "none";
document.getElementById("border-click-17").style.display = "block";

}
function borderclick18() {
document.getElementById("border-click-18").style.display = "none";
document.getElementById("box-click-18").style.display = "block";

}
function boxclick18() {
document.getElementById("box-click-18").style.display = "none";
document.getElementById("border-click-18").style.display = "block";

}
function borderclick19() {
document.getElementById("border-click-19").style.display = "none";
document.getElementById("box-click-19").style.display = "block";

}
function boxclick19() {
document.getElementById("box-click-19").style.display = "none";
document.getElementById("border-click-19").style.display = "block";

}
function borderclick20() {
document.getElementById("border-click-20").style.display = "none";
document.getElementById("box-click-20").style.display = "block";

}
function boxclick20() {
document.getElementById("box-click-20").style.display = "none";
document.getElementById("border-click-20").style.display = "block";

}
function borderclick21() {
document.getElementById("border-click-21").style.display = "none";
document.getElementById("box-click-21").style.display = "block";

}
function boxclick21() {
document.getElementById("box-click-21").style.display = "none";
document.getElementById("border-click-21").style.display = "block";

}
function borderclick22() {
document.getElementById("border-click-22").style.display = "none";
document.getElementById("box-click-22").style.display = "block";

}
function boxclick22() {
document.getElementById("box-click-22").style.display = "none";
document.getElementById("border-click-22").style.display = "block";

}
function borderclick23() {
document.getElementById("border-click-23").style.display = "none";
document.getElementById("box-click-23").style.display = "block";

}
function boxclick23() {
document.getElementById("box-click-23").style.display = "none";
document.getElementById("border-click-23").style.display = "block";

}
function borderclick24() {
document.getElementById("border-click-24").style.display = "none";
document.getElementById("box-click-24").style.display = "block";

}
function boxclick24() {
document.getElementById("box-click-24").style.display = "none";
document.getElementById("border-click-24").style.display = "block";

}
function borderclick25() {
document.getElementById("border-click-25").style.display = "none";
document.getElementById("box-click-25").style.display = "block";

}
function boxclick25() {
document.getElementById("box-click-25").style.display = "none";
document.getElementById("border-click-25").style.display = "block";

}
function borderclick26() {
document.getElementById("border-click-26").style.display = "none";
document.getElementById("box-click-26").style.display = "block";

}
function boxclick26() {
document.getElementById("box-click-26").style.display = "none";
document.getElementById("border-click-26").style.display = "block";

}
function borderclick27() {
document.getElementById("border-click-27").style.display = "none";
document.getElementById("box-click-27").style.display = "block";

}
function boxclick27() {
document.getElementById("box-click-27").style.display = "none";
document.getElementById("border-click-27").style.display = "block";

}
function borderclick28() {
document.getElementById("border-click-28").style.display = "none";
document.getElementById("box-click-28").style.display = "block";

}
function boxclick28() {
document.getElementById("box-click-28").style.display = "none";
document.getElementById("border-click-28").style.display = "block";

}
function borderclick29() {
document.getElementById("border-click-29").style.display = "none";
document.getElementById("box-click-29").style.display = "block";

}
function boxclick29() {
document.getElementById("box-click-29").style.display = "none";
document.getElementById("border-click-29").style.display = "block";

}
function borderclick30() {
document.getElementById("border-click-30").style.display = "none";
document.getElementById("box-click-30").style.display = "block";

}
function boxclick30() {
document.getElementById("box-click-30").style.display = "none";
document.getElementById("border-click-30").style.display = "block";

}
function borderclick31() {
document.getElementById("border-click-31").style.display = "none";
document.getElementById("box-click-31").style.display = "block";

}
function boxclick31() {
document.getElementById("box-click-31").style.display = "none";
document.getElementById("border-click-31").style.display = "block";

}
function borderclick32() {
document.getElementById("border-click-32").style.display = "none";
document.getElementById("box-click-32").style.display = "block";

}
function boxclick32() {
document.getElementById("box-click-32").style.display = "none";
document.getElementById("border-click-32").style.display = "block";

}

function borderclick33() {
document.getElementById("border-click-33").style.display = "none";
document.getElementById("box-click-33").style.display = "block";

}
function boxclick33() {
document.getElementById("box-click-33").style.display = "none";
document.getElementById("border-click-33").style.display = "block";

}

function borderclick34() {
document.getElementById("border-click-34").style.display = "none";
document.getElementById("box-click-34").style.display = "block";

}
function boxclick34() {
document.getElementById("box-click-34").style.display = "none";
document.getElementById("border-click-34").style.display = "block";

}

function borderclick35() {
document.getElementById("border-click-35").style.display = "none";
document.getElementById("box-click-35").style.display = "block";

}
function boxclick35() {
document.getElementById("box-click-35").style.display = "none";
document.getElementById("border-click-35").style.display = "block";

}

function borderclick36() {
document.getElementById("border-click-36").style.display = "none";
document.getElementById("box-click-36").style.display = "block";

}
function boxclick36() {
document.getElementById("box-click-36").style.display = "none";
document.getElementById("border-click-36").style.display = "block";

}

function borderclick37() {
document.getElementById("border-click-37").style.display = "none";
document.getElementById("box-click-37").style.display = "block";

}
function boxclick37() {
document.getElementById("box-click-37").style.display = "none";
document.getElementById("border-click-37").style.display = "block";

}

function borderclick38() {
document.getElementById("border-click-38").style.display = "none";
document.getElementById("box-click-38").style.display = "block";

}
function boxclick38() {
document.getElementById("box-click-38").style.display = "none";
document.getElementById("border-click-38").style.display = "block";

}

function borderclick39() {
document.getElementById("border-click-39").style.display = "none";
document.getElementById("box-click-39").style.display = "block";

}
function boxclick39() {
document.getElementById("box-click-39").style.display = "none";
document.getElementById("border-click-39").style.display = "block";

}

function borderclick40() {
document.getElementById("border-click-40").style.display = "none";
document.getElementById("box-click-40").style.display = "block";

}
function boxclick40() {
document.getElementById("box-click-40").style.display = "none";
document.getElementById("border-click-40").style.display = "block";

}

function borderclick41() {
document.getElementById("border-click-41").style.display = "none";
document.getElementById("box-click-41").style.display = "block";

}
function boxclick41() {
document.getElementById("box-click-41").style.display = "none";
document.getElementById("border-click-41").style.display = "block";

}

function borderclick42() {
document.getElementById("border-click-42").style.display = "none";
document.getElementById("box-click-42").style.display = "block";

}
function boxclick42() {
document.getElementById("box-click-42").style.display = "none";
document.getElementById("border-click-42").style.display = "block";

}

function borderclick43() {
document.getElementById("border-click-43").style.display = "none";
document.getElementById("box-click-43").style.display = "block";

}
function boxclick43() {
document.getElementById("box-click-43").style.display = "none";
document.getElementById("border-click-43").style.display = "block";

}

function borderclick44() {
document.getElementById("border-click-44").style.display = "none";
document.getElementById("box-click-44").style.display = "block";

}
function boxclick44() {
document.getElementById("box-click-44").style.display = "none";
document.getElementById("border-click-44").style.display = "block";

}

function borderclick45() {
document.getElementById("border-click-45").style.display = "none";
document.getElementById("box-click-45").style.display = "block";

}
function boxclick45() {
document.getElementById("box-click-45").style.display = "none";
document.getElementById("border-click-45").style.display = "block";

}

function borderclick46() {
document.getElementById("border-click-46").style.display = "none";
document.getElementById("box-click-46").style.display = "block";

}
function boxclick46() {
document.getElementById("box-click-46").style.display = "none";
document.getElementById("border-click-46").style.display = "block";

}

function borderclick47() {
document.getElementById("border-click-47").style.display = "none";
document.getElementById("box-click-47").style.display = "block";

}
function boxclick47() {
document.getElementById("box-click-47").style.display = "none";
document.getElementById("border-click-47").style.display = "block";

}

function borderclick48() {
document.getElementById("border-click-48").style.display = "none";
document.getElementById("box-click-48").style.display = "block";

}
function boxclick48() {
document.getElementById("box-click-48").style.display = "none";
document.getElementById("border-click-48").style.display = "block";

}

function borderclick49() {
document.getElementById("border-click-49").style.display = "none";
document.getElementById("box-click-49").style.display = "block";

}
function boxclick49() {
document.getElementById("box-click-49").style.display = "none";
document.getElementById("border-click-49").style.display = "block";

}

function borderclick50() {
document.getElementById("border-click-50").style.display = "none";
document.getElementById("box-click-50").style.display = "block";

}
function boxclick50() {
document.getElementById("box-click-50").style.display = "none";
document.getElementById("border-click-50").style.display = "block";

}

function borderclick51() {
document.getElementById("border-click-51").style.display = "none";
document.getElementById("box-click-51").style.display = "block";

}
function boxclick51() {
document.getElementById("box-click-51").style.display = "none";
document.getElementById("border-click-51").style.display = "block";

}

function borderclick52() {
document.getElementById("border-click-52").style.display = "none";
document.getElementById("box-click-52").style.display = "block";

}
function boxclick52() {
document.getElementById("box-click-52").style.display = "none";
document.getElementById("border-click-52").style.display = "block";

}

function borderclick53() {
document.getElementById("border-click-53").style.display = "none";
document.getElementById("box-click-53").style.display = "block";

}
function boxclick53() {
document.getElementById("box-click-53").style.display = "none";
document.getElementById("border-click-53").style.display = "block";

}
function borderclick54() {
document.getElementById("border-click-54").style.display = "none";
document.getElementById("box-click-54").style.display = "block";

}
function boxclick54() {
document.getElementById("box-click-54").style.display = "none";
document.getElementById("border-click-54").style.display = "block";

}
function borderclick55() {
document.getElementById("border-click-55").style.display = "none";
document.getElementById("box-click-55").style.display = "block";

}
function boxclick55() {
document.getElementById("box-click-55").style.display = "none";
document.getElementById("border-click-55").style.display = "block";

}

function fermerTable() {
document.getElementById("fermer-table").style.display = "flex";

}





(function () {
  window.inputNumber = function (el) {
    var min = el.attr("min") || false;
    var max = el.attr("max") || false;

    var els = {};

    els.dec = el.prev();
    els.inc = el.next();

    el.each(function () {
      init($(this));
    });

    function init(el) {
      els.dec.on("click", decrement);
      els.inc.on("click", increment);

      function decrement() {
        var value = el[0].value;
        value--;
        if (!min || value >= min) {
          el[0].value = value;
        }
      }

      function increment() {
        var value = el[0].value;
        value++;
        if (!max || value <= max) {
          el[0].value = value++;
        }
      }
    }
  };
})();

inputNumber($(".input-number"));











var Scrollbar = (function() {

	// Variables

	var $scrollbar = $('.scrollbar-inner');


	// Methods

	function init() {
		$scrollbar.scrollbar().scrollLock()
	}


	// Events

	if ($scrollbar.length) {
		init();
	}

})();




var Cal = function(divId) {

  //Store div id
  this.divId = divId;

  // Days of week, starting on Sunday
  this.DaysOfWeek = [
    'Sun',
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat'
  ];

  // Months, stating on January
  this.Months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];

  // Set the current month, year
  var d = new Date();

  this.currMonth = d.getMonth();
  this.currYear = d.getFullYear();
  this.currDay = d.getDate();

};

// Goes to next month
Cal.prototype.nextMonth = function() {
  if ( this.currMonth == 11 ) {
    this.currMonth = 0;
    this.currYear = this.currYear + 1;
  }
  else {
    this.currMonth = this.currMonth + 1;
  }
  this.showcurr();
};

// Goes to previous month
Cal.prototype.previousMonth = function() {
  if ( this.currMonth == 0 ) {
    this.currMonth = 11;
    this.currYear = this.currYear - 1;
  }
  else {
    this.currMonth = this.currMonth - 1;
  }
  this.showcurr();
};

// Show current month
Cal.prototype.showcurr = function() {
  this.showMonth(this.currYear, this.currMonth);
};

// Show month (year, month)
Cal.prototype.showMonth = function(y, m) {

  var d = new Date()
  // First day of the week in the selected month
  , firstDayOfMonth = new Date(y, m, 1).getDay()
  // Last day of the selected month
  , lastDateOfMonth =  new Date(y, m+1, 0).getDate()
  // Last day of the previous month
  , lastDayOfLastMonth = m == 0 ? new Date(y-1, 11, 0).getDate() : new Date(y, m, 0).getDate();


  var html = '<table>';

  // Write selected month and year
  html += '<thead><tr>';
  html += '<td colspan="7">' + this.Months[m] + ' ' + y + '</td>';
  html += '</tr></thead>';


  // Write the header of the days of the week
  html += '<tr class="days">';
  for(var i=0; i < this.DaysOfWeek.length;i++) {
    html += '<td>' + this.DaysOfWeek[i] + '</td>';
  }
  html += '</tr>';

  // Write the days
  var i=1;
  do {

    var dow = new Date(y, m, i).getDay();

    // If Sunday, start new row
    if ( dow == 0 ) {
      html += '<tr>';
    }
    // If not Sunday but first day of the month
    // it will write the last days from the previous month
    else if ( i == 1 ) {
      html += '<tr>';
      var k = lastDayOfLastMonth - firstDayOfMonth+1;
      for(var j=0; j < firstDayOfMonth; j++) {
        html += '<td class="not-current">' + k + '</td>';
        k++;
      }
    }

    // Write the current day in the loop
    var chk = new Date();
    var chkY = chk.getFullYear();
    var chkM = chk.getMonth();
    if (chkY == this.currYear && chkM == this.currMonth && i == this.currDay) {
      html += '<td class="today">' + i + '</td>';
    } else {
      html += '<td class="normal">' + i + '</td>';
    }
    // If Saturday, closes the row
    if ( dow == 6 ) {
      html += '</tr>';
    }
    // If not Saturday, but last day of the selected month
    // it will write the next few days from the next month
    else if ( i == lastDateOfMonth ) {
      var k=1;
      for(dow; dow < 6; dow++) {
        html += '<td class="not-current">' + k + '</td>';
        k++;
      }
    }

    i++;
  }while(i <= lastDateOfMonth);

  // Closes table
  html += '</table>';

  // Write HTML to the div
  document.getElementById(this.divId).innerHTML = html;
};

// On Load of the window
window.onload = function() {

  // Start calendar
  var c = new Cal("divCal");			
  c.showcurr();

  // Bind next and previous button clicks
  getId('btnNext').onclick = function() {
    c.nextMonth();
  };
  getId('btnPrev').onclick = function() {
    c.previousMonth();
  };
}

// Get element by id
function getId(id) {
  return document.getElementById(id);
}





// DRAG AND DRUP


const draggables = document.querySelectorAll(".draggable");
const containers = document.querySelectorAll(".css-drag");

draggables.forEach((draggable) => {
  draggable.addEventListener("dragstart", () => {
    draggable.classList.add("dragging");
  });

  draggable.addEventListener("dragend", () => {
    draggable.classList.remove("dragging");
  });
});

containers.forEach((container) => {
  container.addEventListener("dragover", (e) => {
    e.preventDefault();
    const afterElement = getDragAfterElement(container, e.clientY);
    const draggable = document.querySelector(".dragging");
    if (afterElement == null) {
      container.appendChild(draggable);
    } else {
      container.insertBefore(draggable, afterElement);
    }
  });
});

function getDragAfterElement(container, y) {
  const draggableElements = [
    ...container.querySelectorAll(".draggable:not(.dragging)")
  ];

  return draggableElements.reduce(
    (closest, child) => {
      const box = child.getBoundingClientRect();
      const offset = y - box.top - box.height / 2;
      if (offset < 0 && offset > closest.offset) {
        return { offset: offset, element: child };
      } else {
        return closest;
      }
    },
    { offset: Number.NEGATIVE_INFINITY }
  ).element;
}

